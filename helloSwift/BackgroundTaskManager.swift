//
//  BackgroundTaskManager.swift
//  helloSwift
//
//  Created by Takuya on 12/29/14.
//  Copyright (c) 2014 WizR. All rights reserved.
//

import Foundation
import UIKit

class BackgroundTaskManager : NSObject {
    private var taskList :[UIBackgroundTaskIdentifier]!
    private var masterTask : UIBackgroundTaskIdentifier!
    
    class var sharedInstance : BackgroundTaskManager {
        struct Static {
            static let instance = BackgroundTaskManager()
        }
        return Static.instance
    }
    
    override init() {
        taskList = []
        masterTask = UIBackgroundTaskInvalid
    }
    
    func beginNewBackgroundTask() -> UIBackgroundTaskIdentifier {
        var application = UIApplication.sharedApplication()
        var bgTask = UIBackgroundTaskInvalid
        
        if application.respondsToSelector("beginBackgroundTaskWithExpirationHandler:") {
            bgTask = application.beginBackgroundTaskWithExpirationHandler({ () -> Void in
                println("background task \(bgTask) expired")
            })
            if masterTask == UIBackgroundTaskInvalid {
                masterTask = bgTask
                println("master task \(bgTask) started")
            } else {
                taskList.append(bgTask)
                println("background task \(bgTask) started")
            }
        }
        
        return bgTask
    }
    
    func endBackgroundTasks() {
        drainTaskList(false)
    }
    
    func endAllBackgroundTasks() {
        drainTaskList(true)
    }
    
    func drainTaskList(all:Bool) {
        var application = UIApplication.sharedApplication()
        if (application.respondsToSelector("endBackgroundTask:")) {
            let count = taskList.count
            let loopCount = all ? count : count - 1
            for _ in 1...loopCount {
                let targetIndex = 0
                var task = taskList[targetIndex]
                application.endBackgroundTask(task)
                taskList.removeAtIndex(targetIndex)
            }
            
            if taskList.count > 0 {
                println("there is still background task \(taskList[0])")
            }
            if all {
                println("there is no background task")
                application.endBackgroundTask(masterTask)
                masterTask = UIBackgroundTaskInvalid
            } else {
                println("master task was kept \(masterTask)")
            }
        }
    }
}