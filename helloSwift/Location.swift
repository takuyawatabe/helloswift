//
//  Location.swift
//  helloSwift
//
//  Created by Takuya on 12/29/14.
//  Copyright (c) 2014 WizR. All rights reserved.
//

import Foundation

class Location: NSObject {
    var latitude:Float!
    var longtitude:Float!
    var accuracy:Float!
    var altitude:Float!
    
    init(latitude: Float, longtitude: Float, accuracy: Float, altitude: Float) {
        self.latitude = latitude
        self.longtitude = longtitude
        self.accuracy = accuracy
        self.altitude = altitude
    }
}