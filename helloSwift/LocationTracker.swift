//
//  LocationTracker.swift
//  helloSwift
//
//  Created by Takuya on 12/29/14.
//  Copyright (c) 2014 WizR. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit

class LocationTracker: NSObject, CLLocationManagerDelegate {
    var locationManager: CLLocationManager! = nil
    var shareModel: ShareModel! = nil
    let timerInterval: NSTimeInterval = 60.0
    let delayedTimerInterval: NSTimeInterval = 10.0
    
    class var sharedInstance : LocationTracker {
        struct Static {
            static let instance = LocationTracker()
        }
        return Static.instance
    }
    
    override init() {
        super.init()
        
        locationManager = CLLocationManager()
        shareModel = ShareModel.sharedInstance
        shareModel.bgManager = BackgroundTaskManager.sharedInstance
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector:"applicationEnterBackground:", name: UIApplicationDidEnterBackgroundNotification, object: [])
    }
    
    func restartLocationUpdates() {
        println("restart location upates")
        
        shareModel.timer?.invalidate()
        shareModel.timer = nil
        
        configureLocationManager()
    }
    
    func startTracking() {
        if (!CLLocationManager.locationServicesEnabled()) {
            alertLocationServiceDisabled()
            return
        }
        var authorizationStatus = CLLocationManager.authorizationStatus()
        switch (authorizationStatus) {
        case .Denied:
            alertLocationServiceDisabled()
        case .Restricted:
            alertLocationServiceDisabled()
        default:
            break
        }
        
        configureLocationManager()
        locationManager.startUpdatingLocation()
    }
    
    private func configureLocationManager() {
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
        locationManager.distanceFilter = kCLDistanceFilterNone;
        
        locationManager.requestAlwaysAuthorization()
    }
    
    func stopTracking() {
        shareModel.timer?.invalidate()
        shareModel.timer = nil;
        locationManager.stopUpdatingLocation()
    }
    
    func applicationEnterBackground() {
        println("enterbackground")
    }
    
    func alertLocationServiceDisabled() {
        let title = "Location Serverice Disabled"
        let message = "Please enable Location Service to enable this feature"
        let alert = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: "Setting", style: .Default, handler: { action in
            let url = NSURL(string: UIApplicationOpenSettingsURLString)
            UIApplication.sharedApplication().openURL(url!)
        }))
        alert.addAction(UIAlertAction(title: "Close", style: .Cancel, handler: nil))
        UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(alert, animated: true, completion: nil)
    }
    
    // MARK: - CLLocationManagerDelegate
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        println("locationManager didUpdateLocations")
        for location in locations as [CLLocation] {
            var locationAge : NSTimeInterval = -1 * location.timestamp.timeIntervalSinceNow
            if locationAge > 30.0 {
                continue
            }
            var coordinate = location.coordinate
            var altitude = location.altitude
            var accuracy = location.horizontalAccuracy
            
            if accuracy > 0 && accuracy < 2000 && coordinate.latitude != 0.0 && coordinate.longitude != 0.0 {                
                var myLocation = Location(
                    latitude: NSNumber(double: coordinate.latitude) as Float,
                    longtitude: NSNumber(double: coordinate.longitude) as Float,
                    accuracy: NSNumber(double: accuracy) as Float,
                    altitude: NSNumber(double: altitude) as Float
                )
                shareModel.locationList.append(myLocation)
                println("lat: \(myLocation.latitude), lon: \(myLocation.longtitude)")
                
                var parseUser = PFUser.currentUser()
                if parseUser != nil {
                    var parseObject = PFObject(className: "Location")
                    parseObject.setObject(myLocation.longtitude, forKey: "longtitude")
                    parseObject.setObject(myLocation.latitude, forKey: "latitude")
                    parseObject.setObject(myLocation.altitude, forKey: "altitude")
                    parseObject.setObject(myLocation.accuracy, forKey: "accuracy")
                    parseObject.setObject(parseUser.objectId, forKey: "user_id")
                    parseObject.saveInBackgroundWithBlock({ (success: Bool, error: NSError!) -> Void in
                        if success {
                            println("Object created with id: \(parseObject.objectId)")
                        } else {
                            println("error: \(error.description)")
                        }
                    })
                }
            }
        }
        
        if shareModel.timer != nil {
            return
        }
        
        shareModel.bgManager = BackgroundTaskManager.sharedInstance
        shareModel.bgManager.beginNewBackgroundTask()
        
        shareModel.timer = NSTimer.scheduledTimerWithTimeInterval(timerInterval, target: self, selector: "restartLocationUpdates", userInfo: nil, repeats: false)
        
        shareModel.delayedTimer?.invalidate()
        shareModel.delayedTimer = NSTimer(timeInterval: delayedTimerInterval, target: self, selector: "stopLocationDelay", userInfo: nil, repeats: false)
    }
    
    func locationManager(manager: CLLocationManager!, didFailWithError error: NSError!) {
        println("error \(error.description) [\(error.code)]")
    }
    
    func stopLocationDelay() {
        locationManager.stopUpdatingLocation()
    }
}