//
//  MapViewController.swift
//  helloSwift
//
//  Created by Takuya on 12/30/14.
//  Copyright (c) 2014 WizR. All rights reserved.
//

import UIKit

class MapViewController: UIViewController {
    @IBOutlet weak var mapView: GMSMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mapView.hidden = true
        
        var query = PFQuery(className: "Location")
        query.whereKey("user_id", equalTo: PFUser.currentUser().objectId)
        query.orderByDescending("createdAt")
        query.findObjectsInBackgroundWithBlock { (objects:[AnyObject]!, error:NSError!) -> Void in
            if error == nil {
                var locationList: [Location] = []
                for object in objects {
                    var latitude : Float = object.objectForKey("latitude") as Float
                    var longtitude : Float = object.objectForKey("longtitude") as Float
                    var altitude : Float = object.objectForKey("altitude") as Float
                    var accuracy : Float = object.objectForKey("accuracy") as Float
                    
                    var location = Location(latitude: latitude, longtitude: longtitude, accuracy: accuracy, altitude: altitude)
                    locationList.append(location)
                }
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.displayMap(locationList)
                })
            } else {
                println("pf query error : \(error.userInfo)")
            }
        }
    }
    
    @IBAction func closeButtonPushed(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func displayMap(locationList:[Location]) {
        var firstLocation = locationList[0]
        var firstPosition = CLLocationCoordinate2DMake(Double(firstLocation.latitude), Double(firstLocation.longtitude))
        self.mapView.animateToLocation(firstPosition)
        var firstMarker = GMSMarker()
        firstMarker.position = firstPosition
        firstMarker.map = self.mapView
        
        var path = GMSMutablePath()
        var bounds = GMSCoordinateBounds(coordinate: firstPosition, coordinate: firstPosition)
        for location in locationList {
            var position = CLLocationCoordinate2DMake(Double(location.latitude), Double(location.longtitude))
            bounds = bounds.includingCoordinate(position)
            path.addLatitude(position.latitude, longitude: position.longitude)
        }
        
        var polyline = GMSPolyline(path: path)
        polyline.strokeColor = UIColor.blueColor()
        polyline.strokeWidth = 5.0
        polyline.map = self.mapView
        
        var updateCamera = GMSCameraUpdate.fitBounds(bounds, withPadding: 50.0)
        self.mapView.moveCamera(updateCamera)
        
        self.mapView.hidden = false
    }
}