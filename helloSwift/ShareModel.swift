//
//  ShareModel.swift
//  helloSwift
//
//  Created by Takuya on 12/29/14.
//  Copyright (c) 2014 WizR. All rights reserved.
//

import Foundation

class ShareModel: NSObject {
    var timer: NSTimer?
    var delayedTimer: NSTimer?
    var bgManager: BackgroundTaskManager!
    var locationList: [Location] = []
    
    class var sharedInstance : ShareModel {
        struct Static {
            static var token: dispatch_once_t = 0
            static var instance: ShareModel?
        }
        dispatch_once(&Static.token) {
            Static.instance = ShareModel()
        }
        return Static.instance!
    }
}