//
//  ViewController.swift
//  helloSwift
//
//  Created by Takuya on 12/29/14.
//  Copyright (c) 2014 WizR. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var tracker: LocationTracker!

    @IBOutlet weak var startBtutton: UIButton!
    @IBOutlet weak var stopButton: UIButton!
    @IBOutlet weak var viewMapButton: UIButton!
    
    @IBOutlet weak var statusText: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        tracker = LocationTracker()
        self.stopButton.enabled = false
        
        self.startBtutton.layer.cornerRadius = 3
        self.stopButton.layer.cornerRadius = 3
        self.viewMapButton.layer.cornerRadius = 3
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func startButtonPushed(sender: AnyObject) {
        tracker.startTracking()
        self.startBtutton.enabled = false
        self.stopButton.enabled = true
        self.statusText.text = "Logging Status: Started"
    }
    @IBAction func stopButtonPushed(sender: AnyObject) {
        tracker.stopTracking()
        self.startBtutton.enabled = true
        self.stopButton.enabled = false
        self.statusText.text = "Logging Status: Stopped"
    }
}

